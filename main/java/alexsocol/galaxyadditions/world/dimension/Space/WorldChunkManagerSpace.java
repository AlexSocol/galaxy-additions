package alexsocol.galaxyadditions.world.dimension.Space;

import galaxyspace.SolarSystem.core.world.gen.GSBiomeGenBase;
import galaxyspace.SolarSystem.moons.oberon.dimension.WorldChunkManagerOberon;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraft.world.biome.WorldChunkManager;

public class WorldChunkManagerSpace extends micdoodle8.mods.galacticraft.api.prefab.world.gen.WorldChunkManagerSpace {

	public WorldChunkManagerSpace() { }

	public BiomeGenBase getBiome() {
		return GSBiomeGenBase.GSSpace;
	}

}