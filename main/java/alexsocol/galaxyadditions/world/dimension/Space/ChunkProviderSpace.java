package alexsocol.galaxyadditions.world.dimension.Space;

import java.util.List;

import alexsocol.asjlib.ChunkProviderFlat;
import alexsocol.galaxyadditions.utils.RegistrationsList;
import micdoodle8.mods.galacticraft.core.world.gen.ChunkProviderOrbit;
import micdoodle8.mods.galacticraft.core.world.gen.WorldGenSpaceStation;
import net.minecraft.block.Block;
import net.minecraft.block.BlockFalling;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.init.Blocks;
import net.minecraft.util.IProgressUpdate;
import net.minecraft.world.ChunkPosition;
import net.minecraft.world.World;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.IChunkProvider;

public class ChunkProviderSpace extends ChunkProviderFlat {

	public ChunkProviderSpace(World world, long seed, boolean features) {
		super(world, seed, "2;255x0;1");
	}

	@Override
    public void populate(IChunkProvider p_73153_1_, int x, int z) {
        int k = x * 16;
        int l = z * 16;
        BiomeGenBase biomegenbase = this.worldObj.getBiomeGenForCoords(k + 16, l + 16);
        boolean flag = false;
        this.random.setSeed(this.worldObj.getSeed());
        long i1 = this.random.nextLong() / 2L * 2L + 1L;
        long j1 = this.random.nextLong() / 2L * 2L + 1L;
        if(k == 0 && l == 0){
        	new WorldGenSpaceStation().generate(worldObj, random, k, 70, l);
        }
        this.random.setSeed((long)x * i1 + (long)z * j1 ^ this.worldObj.getSeed());
    }
}
