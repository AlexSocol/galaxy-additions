//- By Vamig Aliev.
//- https://vk.com/win_vista.

package alexsocol.galaxyadditions.world.dimension.JupiterNew;

import java.util.Random;

import alexsocol.galaxyadditions.utils.RegistrationsList;
import net.minecraft.init.Blocks;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import vamig.worldengine.WE_CreateChunkGen;
import vamig.worldengine.WE_GeneratorData;
import vamig.worldengine.WE_PerlinNoise;

public class WE_JupiterGenerator extends WE_CreateChunkGen {
	@Override
	public void gen(WE_GeneratorData data) {
		for(int x = 0; x < 16; x++)
			for(int z = 0; z < 16; z++) {
				double n = WE_PerlinNoise.PerlinNoise2D(data.chunkProvider.worldObj.getSeed(),
					(data.chunk_X + (long)x) / 16.0D, (data.chunk_Z + (long)z) / 16.0D, 2.1D, 3);
				for(int y = 0; y < 256; y++)
					if     (y ==   0                                    )
						setBlock(data, Blocks.bedrock             , (byte)0, x, y, z);
					else if(y <=  40 + MathHelper.floor_double(n * 2.0D))
						setBlock(data, Blocks.stone               , (byte)0, x, y, z);
					else if(y <= 100 + MathHelper.floor_double(n       ))
						setBlock(data, RegistrationsList.HMetal   , (byte)0, x, y, z);
					else if(y <= 160                                    )
						setBlock(data, RegistrationsList.HHeLiquid, (byte)0, x, y, z);
					else if(y >= 246 && y <= 255
						&&  y >= 248 + MathHelper.floor_double(n * 3.5D)
						&&  y <= 252 - MathHelper.floor_double(n * 3.0D))
						setBlock(data, RegistrationsList.HCloud   , (byte)0, x, y, z);
			}
	}
	
	@Override
	public void generate(Random random, int chunkX, int chunkZ, World world, IChunkProvider chunkGenerator, IChunkProvider chunkProvider) {
		
	}
}