package alexsocol.galaxyadditions;

/** Use them to easily enable/disable whole functions of the addon */
public class Switches {
	
	/** Change this to false if you getting crashes when loading obj models */
	public static final boolean parseObj = true;
	
	/** Change this to operate space dimension functionals */
	public static final boolean spaceDim = true;
}
