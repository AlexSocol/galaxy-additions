package alexsocol.galaxyadditions.utils;

import alexsocol.galaxyadditions.GAMain;
import net.minecraftforge.common.config.Configuration;

public class GAConfig extends Configuration {
	// Categories
	public static final String DIMIDS = Configuration.CATEGORY_GENERAL + Configuration.CATEGORY_SPLITTER + "Dimension IDs";
	public static final String BIOMEIDS = Configuration.CATEGORY_GENERAL + Configuration.CATEGORY_SPLITTER + "Biome IDs";
	public static final String PLANETS = Configuration.CATEGORY_GENERAL + Configuration.CATEGORY_SPLITTER + "Planets";
	public static final String FEATURES = Configuration.CATEGORY_GENERAL + Configuration.CATEGORY_SPLITTER + "Features";
	
	// DIM IDs
	public static int space = 9000;
	public static int jupiter = 100;
	public static int saturn = 101;
	public static int saturnRings = 200;
	public static int uranus = 102;
	public static int neptune = 103;
	public static int thanatos = -1000;
	public static int newYear = 1000;
	public static int halloween = 666;

	// Biome IDs
	public static int BiomeJupiter = 50;
	public static int BiomeSaturn = 51;
	public static int BiomeSaturnRings = 52;
	public static int BiomeUranus = 53;
	public static int BiomeNeptune = 54;
	public static int BiomeThanatos = 55;
	public static int BiomeNewYear = 56;
	public static int BiomeHalloween = 57;
	
	// Planets
	public static boolean isNewYear = false;
	public static boolean isHalloween = false;
	
	// Features
	public static boolean isSaturnDiamondRain = true;
	public static boolean isOverworldMeteors = true;
	
	public static double debugscale = 1;
	/** Absolute Star Size */
	public static double StarSize = 16384.0;
	/** Absolute Planet Size */
	public static double PlanetSize = 1024.0;
	/** Absolute Moon Size */
	public static double MoonSize = 192.0;
	/** Absolute Star <-> Star Distance */
	public static double StarStarDistance = 1000.0;
	/** Absolute Star <-> Planet Distance */
	public static double StarPlanetDistance = 65536.0;
	/** Absolute Planet <-> Moon Distance */
	public static double PlanetMoonDistance = 2048.0;

	public static void syncConfig() {
		GAMain.config.addCustomCategoryComment(DIMIDS, "Change dimension IDs here");
		GAMain.config.addCustomCategoryComment(BIOMEIDS, "Change biome IDs here");
		GAMain.config.addCustomCategoryComment(PLANETS, "Disable unwanted planets here here");
		GAMain.config.addCustomCategoryComment(FEATURES, "Turn off features here");
		
		space = GAMain.config.getInt("Space dimension ID", DIMIDS, space, Integer.MIN_VALUE/2, Integer.MAX_VALUE/2, "ID of Space dimension");
		
		jupiter = GAMain.config.get(DIMIDS, "Jupiter dimension ID", jupiter).getInt(jupiter);
		saturn = GAMain.config.get(DIMIDS, "Saturn dimension ID", saturn).getInt(saturn);
		saturnRings = GAMain.config.get(DIMIDS, "Saturn Rings dimension ID", saturnRings).getInt(saturnRings);
		uranus = GAMain.config.get(DIMIDS, "Uranus dimension ID", uranus).getInt(uranus);
		neptune = GAMain.config.get(DIMIDS, "Neptune dimension ID", neptune).getInt(neptune);
		thanatos = GAMain.config.get(DIMIDS, "Thanatos dimension ID", thanatos).getInt(thanatos);
		newYear = GAMain.config.get(DIMIDS, "New Year Planet dimension ID", newYear).getInt(newYear);
		halloween = GAMain.config.get(DIMIDS, "Halloween Planet dimension ID", halloween).getInt(halloween);

		BiomeJupiter = GAMain.config.get(BIOMEIDS, "Jupiter biome ID", BiomeJupiter).getInt(BiomeJupiter);
		BiomeSaturn = GAMain.config.get(BIOMEIDS, "Saturn biome ID", BiomeSaturn).getInt(BiomeSaturn);
		BiomeSaturnRings = GAMain.config.get(BIOMEIDS, "Saturn Rings biome ID", BiomeSaturnRings).getInt(BiomeSaturnRings);
		BiomeUranus = GAMain.config.get(BIOMEIDS, "Uranus biome ID", BiomeUranus).getInt(BiomeUranus);
		BiomeNeptune = GAMain.config.get(BIOMEIDS, "Neptune biome ID", BiomeNeptune).getInt(BiomeNeptune);
		BiomeThanatos = GAMain.config.get(BIOMEIDS, "Thanatos biome ID", BiomeThanatos).getInt(BiomeThanatos);
		BiomeNewYear = GAMain.config.get(BIOMEIDS, "New Year Planet biome ID", BiomeNewYear).getInt(BiomeNewYear);
		BiomeHalloween = GAMain.config.get(BIOMEIDS, "Halloween Planet biome ID", BiomeHalloween).getInt(BiomeHalloween);
		
		isNewYear = GAMain.config.get(PLANETS, "Override calendar for New Year Planet", isNewYear).getBoolean(isNewYear);
		isHalloween = GAMain.config.get(PLANETS, "Override calendar for Halloween Planet", isHalloween).getBoolean(isHalloween);

		isSaturnDiamondRain = GAMain.config.get(FEATURES, "Enable Diamond rain on Saturn", isSaturnDiamondRain).getBoolean(isSaturnDiamondRain);
		isOverworldMeteors = GAMain.config.get(FEATURES, "Enable meteors in Overworld", isOverworldMeteors).getBoolean(isOverworldMeteors);

		StarSize = GAMain.config.get(FEATURES, "Absolute Star Size", StarSize).getDouble(StarSize);
		PlanetSize = GAMain.config.get(FEATURES, "Absolute Planet Size", PlanetSize).getDouble(PlanetSize);
		MoonSize = GAMain.config.get(FEATURES, "Absolute Moon Size", MoonSize).getDouble(MoonSize);
		StarStarDistance = GAMain.config.get(FEATURES, "Absolute Star <-> Star Distance", StarStarDistance).getDouble(StarStarDistance);
		StarPlanetDistance = GAMain.config.get(FEATURES, "Absolute Star <-> Planet Distance", StarPlanetDistance).getDouble(StarPlanetDistance);
		PlanetMoonDistance = GAMain.config.get(FEATURES, "AAbsolute Planet <-> Moon Distance", PlanetMoonDistance).getDouble(PlanetMoonDistance);
		
		if (GAMain.config.hasChanged()) {
			GAMain.config.save();
		}
	}
}