package alexsocol.galaxyadditions;

public class ModInfo {

	public static final String MODID = "galaxyadditions";
	public static final String MIDP = MODID + ':';
	public static final String NAME = "Galaxy Additions";
	public static final String VERSION = "v1b9";
}
