package alexsocol.galaxyadditions.proxy;

import alexsocol.galaxyadditions.GAMain;
import alexsocol.galaxyadditions.ModInfo;
import alexsocol.galaxyadditions.event.EventReactions;
import alexsocol.galaxyadditions.event.MessageActions;
import alexsocol.galaxyadditions.proxy.network.CommonEventHandler;
import alexsocol.galaxyadditions.utils.RegistrationsList;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.block.Block;
import net.minecraftforge.common.MinecraftForge;

public class CommonProxy {

	public RegistrationsList regs;
	public static SimpleNetworkWrapper network;
	
	public CommonProxy() {
		regs = new RegistrationsList();
	}
	
	public void registerRenderThings() {}
	public void RegisterKeyBinds() {}
	
	public int getBlockRender(Block id){
        return -1;
    }
    
	public void preInit() {
		network = NetworkRegistry.INSTANCE.newSimpleChannel(ModInfo.MODID);
		network.registerMessage(MessageActions.Handler.class, MessageActions.class, 0, Side.SERVER);
		regs.construct();
    	regs.RegisterBlocks();
    	regs.RegisterFluids();
    	regs.RegisterItems();
    	regs.RegisterArmor();
    	regs.RegisterTileEntities();
    	regs.RegisterEntities();
    	regs.RegisterRecipes();
	}
	
    public void initializeAndRegisterHandlers() {
    	FMLCommonHandler.instance().bus().register(GAMain.instance);
    	MinecraftForge.EVENT_BUS.register(new CommonEventHandler());
		FMLCommonHandler.instance().bus().register(new CommonEventHandler());
    }

	public void init() {
		regs.constructSpace();
		regs.RegisterPlanets();
    	regs.RegisterMoons();
		FMLCommonHandler.instance().bus().register(new EventReactions());
	}
	
	public void postInit() {
		regs.RegisterRecipesPost();
	}
}
