package alexsocol.galaxyadditions.blocks;

import alexsocol.galaxyadditions.GAMain;
import alexsocol.galaxyadditions.ModInfo;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;

public class HCloud extends Block {
	
	public HCloud() {
		super(Material.cloth);
		this.setBlockName("HCloud");
		this.setBlockTextureName(ModInfo.MODID + ":HCloud");
		this.setCreativeTab(GAMain.gaTab);
		this.setLightOpacity(0);
		this.setHardness(4.0F);
		this.setStepSound(soundTypeCloth);
	}
	
	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int par2, int par3, int par4) {
		return null;
	}
}
