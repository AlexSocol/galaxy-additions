package alexsocol.galaxyadditions.blocks;

import alexsocol.galaxyadditions.GAMain;
import alexsocol.galaxyadditions.ModInfo;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class H2ONH3CH4Ice extends Block {

	public H2ONH3CH4Ice() {
		super(Material.packedIce);
		this.setBlockName("H2ONH3CH4Ice");
        this.setBlockTextureName(ModInfo.MODID + ":H2ONH3CH4Ice");
        this.setCreativeTab(GAMain.gaTab);
        this.setHardness(4.0F);
        this.setHarvestLevel("pickaxe", 2);
        this.setResistance(200.0F);
        this.slipperiness = 0.95F;
        this.setStepSound(soundTypeGlass);
	}
}