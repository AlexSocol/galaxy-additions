package alexsocol.galaxyadditions.blocks.render;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;
import galaxyspace.GalaxySpace;
import galaxyspace.BarnardsSystem.BarnardsPlanets;
import galaxyspace.SolarSystem.SolarSystemPlanets;
import micdoodle8.mods.galacticraft.api.galaxies.CelestialBody;
import micdoodle8.mods.galacticraft.api.galaxies.GalaxyRegistry;
import micdoodle8.mods.galacticraft.api.galaxies.Moon;
import micdoodle8.mods.galacticraft.api.galaxies.Planet;
import micdoodle8.mods.galacticraft.api.galaxies.SolarSystem;
import micdoodle8.mods.galacticraft.core.GalacticraftCore;
import micdoodle8.mods.galacticraft.planets.GalacticraftPlanets;
import micdoodle8.mods.galacticraft.planets.asteroids.AsteroidsModule;
import micdoodle8.mods.galacticraft.planets.mars.MarsModule;
import micdoodle8.mods.galacticraft.planets.mars.dimension.WorldProviderMars;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

public class HoloMapRender extends TileEntitySpecialRenderer {

	@Override
	public void renderTileEntityAt(TileEntity te, double x, double y, double z, float f) {
		GL11.glPushMatrix();
		GL11.glDisable(GL11.GL_CULL_FACE);
		GL11.glTranslated(x, y, z);

		GL11.glDisable(GL11.GL_LIGHTING);
        OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 240, 240);
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        
        Iterator iterator = GalaxyRegistry.getRegisteredSolarSystems().values().iterator();
        
        while (iterator.hasNext()) {
        	Object system = iterator.next();
        	if (system instanceof SolarSystem) {
        		renderSystem((SolarSystem)system);
        	}
        }
        
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glPopMatrix();
	}
	
	public void renderSystem(SolarSystem system) {
		GL11.glPushMatrix();
		GL11.glTranslated(system.getMapPosition().x / 75.0 + 0.5, 1.5, system.getMapPosition().y / 75.0 + 0.5);
		GL11.glScaled(0.25, 0.25, 0.25);
		double s = system.getMainStar().getRelativeSize();
		GL11.glScaled(s, s, s);
		Tessellator tes = Tessellator.instance;
		Minecraft.getMinecraft().renderEngine.bindTexture(system.getMainStar().getBodyIcon());
		tes.startDrawingQuads();
		
		tes.addVertexWithUV(-0.5, -0.5, 0.5, 0, 1);
        tes.addVertexWithUV(-0.5, -0.5, -0.5, 0, 0);
        tes.addVertexWithUV(0.5, -0.5, -0.5, 1, 0);
        tes.addVertexWithUV(0.5, -0.5, 0.5, 1, 1);
		
		tes.addVertexWithUV(0.5, 0.5, 0.5, 1, 1);
        tes.addVertexWithUV(0.5, 0.5, -0.5, 1, 0);
        tes.addVertexWithUV(-0.5, 0.5, -0.5, 0, 0);
        tes.addVertexWithUV(-0.5, 0.5, 0.5, 0, 1);
		
		tes.addVertexWithUV(-0.5, 0.5, -0.5, 1, 0);
        tes.addVertexWithUV(0.5, 0.5, -0.5, 0, 0);
        tes.addVertexWithUV(0.5, -0.5, -0.5, 0, 1);
        tes.addVertexWithUV(-0.5, -0.5, -0.5, 1, 1);
		
		tes.addVertexWithUV(-0.5, 0.5, 0.5, 0, 0);
        tes.addVertexWithUV(-0.5, -0.5, 0.5, 0, 1);
        tes.addVertexWithUV(0.5, -0.5, 0.5, 1, 1);
        tes.addVertexWithUV(0.5, 0.5, 0.5, 1, 0);
		
		tes.addVertexWithUV(-0.5, 0.5, 0.5, 1, 0);
        tes.addVertexWithUV(-0.5, 0.5, -0.5, 0, 0);
        tes.addVertexWithUV(-0.5, -0.5, -0.5, 0, 1);
        tes.addVertexWithUV(-0.5, -0.5, 0.5, 1, 1);
		
		tes.addVertexWithUV(0.5, -0.5, 0.5, 0, 1);
        tes.addVertexWithUV(0.5, -0.5, -0.5, 1, 1);
        tes.addVertexWithUV(0.5, 0.5, -0.5, 1, 0);
        tes.addVertexWithUV(0.5, 0.5, 0.5, 0, 0);
		
		tes.draw();
		GL11.glPopMatrix();
		
		GL11.glPushMatrix();
		GL11.glTranslated(system.getMapPosition().x / 75.0, 0, system.getMapPosition().y / 75.0);
		renderAllPlanets(system);
		GL11.glPopMatrix();
	}
	
	public void renderAllPlanets(SolarSystem system) {
		List planets = GalaxyRegistry.getPlanetsForSolarSystem(system);
		Iterator iterator = planets.iterator();
		
		while (iterator.hasNext()) {
			renderPlanet((Planet)iterator.next());
		}
	}

	public void renderPlanet(Planet planet) {
		GL11.glPushMatrix();
		Tessellator tes = Tessellator.instance;
		Minecraft.getMinecraft().renderEngine.bindTexture(planet.getBodyIcon());
		
		renderRing(planet);
		
		GL11.glTranslated(0.5, 1.5, 0.5);
		GL11.glRotated(planet.getPhaseShift() * (180 / Math.PI), 0, 1, 0);
		GL11.glRotated((double)(Minecraft.getMinecraft().theWorld.getWorldTime()) % (1200.0 * planet.getRelativeOrbitTime()) * 360.0 / (1200.0 * planet.getRelativeOrbitTime()), 0, 1, 0);
		GL11.glTranslated(planet.getRelativeDistanceFromCenter().scaledDistance, 0, 0);
// TODO: spin timer		GL11.glRotated((double)(Minecraft.getMinecraft().theWorld.getWorldTime() % 24000) * 360.0 / 24000.0, 0, 1, 0);
		GL11.glScaled(0.1, 0.1, 0.1);
		double s = planet.getRelativeSize();
		GL11.glScaled(s, s, s);
		tes.startDrawingQuads();
		
        tes.addVertexWithUV(-0.5, -0.5, 0.5, 0, 1);
        tes.addVertexWithUV(-0.5, -0.5, -0.5, 0, 0);
        tes.addVertexWithUV(0.5, -0.5, -0.5, 1, 0);
        tes.addVertexWithUV(0.5, -0.5, 0.5, 1, 1);
		
		tes.addVertexWithUV(0.5, 0.5, 0.5, 1, 1);
        tes.addVertexWithUV(0.5, 0.5, -0.5, 1, 0);
        tes.addVertexWithUV(-0.5, 0.5, -0.5, 0, 0);
        tes.addVertexWithUV(-0.5, 0.5, 0.5, 0, 1);
		
		tes.addVertexWithUV(-0.5, 0.5, -0.5, 1, 0);
        tes.addVertexWithUV(0.5, 0.5, -0.5, 0, 0);
        tes.addVertexWithUV(0.5, -0.5, -0.5, 0, 1);
        tes.addVertexWithUV(-0.5, -0.5, -0.5, 1, 1);
		
		tes.addVertexWithUV(-0.5, 0.5, 0.5, 0, 0);
        tes.addVertexWithUV(-0.5, -0.5, 0.5, 0, 1);
        tes.addVertexWithUV(0.5, -0.5, 0.5, 1, 1);
        tes.addVertexWithUV(0.5, 0.5, 0.5, 1, 0);
		
		tes.addVertexWithUV(-0.5, 0.5, 0.5, 1, 0);
        tes.addVertexWithUV(-0.5, 0.5, -0.5, 0, 0);
        tes.addVertexWithUV(-0.5, -0.5, -0.5, 0, 1);
        tes.addVertexWithUV(-0.5, -0.5, 0.5, 1, 1);
		
		tes.addVertexWithUV(0.5, -0.5, 0.5, 0, 1);
        tes.addVertexWithUV(0.5, -0.5, -0.5, 1, 1);
        tes.addVertexWithUV(0.5, 0.5, -0.5, 1, 0);
        tes.addVertexWithUV(0.5, 0.5, 0.5, 0, 0);
		
		tes.draw();
		GL11.glPopMatrix();
		
		GL11.glPushMatrix();
		GL11.glTranslated(0.5, 1.5, 0.5);
		GL11.glRotated(planet.getPhaseShift() * (180 / Math.PI), 0, 1, 0);
		GL11.glRotated((double)(Minecraft.getMinecraft().theWorld.getWorldTime() % (1200 * planet.getRelativeOrbitTime())) * 360.0 / (1200.0 * planet.getRelativeOrbitTime()), 0, 1, 0);
		GL11.glTranslated(planet.getRelativeDistanceFromCenter().scaledDistance, 0, 0);
		renderAllMoons(planet);
		GL11.glPopMatrix();
	}

	public void renderRing(Planet planet) {
		GL11.glPushMatrix();

		GL11.glTranslated(0.5, 1.5, 0.5);
		GL11.glRotated(90.0, 1.0, 0.0, 0.0);
		GL11.glScaled(planet.getRelativeDistanceFromCenter().scaledDistance, planet.getRelativeDistanceFromCenter().scaledDistance, 1);
		
		GL11.glBegin(GL11.GL_LINE_LOOP);
		for(int i = 0; i <= 360; i++) {
			double angle = 2 * Math.PI * i / 360;
			double x = Math.cos(angle);
			double y = Math.sin(angle);
			GL11.glVertex2d(x, y);
		}
		GL11.glEnd();
		
		GL11.glPopMatrix();		
	}
	
	public void renderAllMoons(Planet planet) {
		List moons = GalaxyRegistry.getMoonsForPlanet(planet);
		Iterator iterator = moons.iterator();
		
		while (iterator.hasNext()) {
			renderMoon((Moon)iterator.next());
		}
	}

	public void renderMoon(Moon moon) {
		GL11.glPushMatrix();
		Tessellator tes = Tessellator.instance;
		Minecraft.getMinecraft().renderEngine.bindTexture(moon.getBodyIcon());
		
		GL11.glRotated(moon.getPhaseShift() * (180 / Math.PI), 0, 1, 0);
		GL11.glRotated((double)(Minecraft.getMinecraft().theWorld.getWorldTime() % (12 * moon.getRelativeOrbitTime())) * 360.0 / (12.0 * moon.getRelativeOrbitTime()), 0, 1, 0);
		GL11.glTranslated(moon.getRelativeDistanceFromCenter().scaledDistance / 150, 0, 0);
// TODO: spin timer		GL11.glRotated((double)(Minecraft.getMinecraft().theWorld.getWorldTime() % 24000) * 360.0 / 24000.0, 0, 1, 0);
		GL11.glScaled(0.01, 0.01, 0.01);
		tes.startDrawingQuads();
		
        tes.addVertexWithUV(-0.5, -0.5, 0.5, 0, 1);
        tes.addVertexWithUV(-0.5, -0.5, -0.5, 0, 0);
        tes.addVertexWithUV(0.5, -0.5, -0.5, 1, 0);
        tes.addVertexWithUV(0.5, -0.5, 0.5, 1, 1);
		
		tes.addVertexWithUV(0.5, 0.5, 0.5, 1, 1);
        tes.addVertexWithUV(0.5, 0.5, -0.5, 1, 0);
        tes.addVertexWithUV(-0.5, 0.5, -0.5, 0, 0);
        tes.addVertexWithUV(-0.5, 0.5, 0.5, 0, 1);
		
		tes.addVertexWithUV(-0.5, 0.5, -0.5, 1, 0);
        tes.addVertexWithUV(0.5, 0.5, -0.5, 0, 0);
        tes.addVertexWithUV(0.5, -0.5, -0.5, 0, 1);
        tes.addVertexWithUV(-0.5, -0.5, -0.5, 1, 1);
		
		tes.addVertexWithUV(-0.5, 0.5, 0.5, 0, 0);
        tes.addVertexWithUV(-0.5, -0.5, 0.5, 0, 1);
        tes.addVertexWithUV(0.5, -0.5, 0.5, 1, 1);
        tes.addVertexWithUV(0.5, 0.5, 0.5, 1, 0);
		
		tes.addVertexWithUV(-0.5, 0.5, 0.5, 1, 0);
        tes.addVertexWithUV(-0.5, 0.5, -0.5, 0, 0);
        tes.addVertexWithUV(-0.5, -0.5, -0.5, 0, 1);
        tes.addVertexWithUV(-0.5, -0.5, 0.5, 1, 1);
		
		tes.addVertexWithUV(0.5, -0.5, 0.5, 0, 1);
        tes.addVertexWithUV(0.5, -0.5, -0.5, 1, 1);
        tes.addVertexWithUV(0.5, 0.5, -0.5, 1, 0);
        tes.addVertexWithUV(0.5, 0.5, 0.5, 0, 0);
		
		tes.draw();
		GL11.glPopMatrix();
	}
}
