package alexsocol.galaxyadditions.entity.render;

import java.util.Random;

import org.lwjgl.opengl.GL11;

import alexsocol.galaxyadditions.ModInfo;
import alexsocol.galaxyadditions.entity.IonPlasmaBurstEntity;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.AdvancedModelLoader;
import net.minecraftforge.client.model.IModelCustom;

public class RenderFire extends Render {
	
	public static final IModelCustom sphere = AdvancedModelLoader.loadModel(new ResourceLocation(ModInfo.MODID, "model/IonPlasmaSphere.obj"));
	
    public RenderFire() {
        shadowSize = 0.0F;
    }

    @Override
    protected ResourceLocation getEntityTexture(Entity entity) {
        return new ResourceLocation(ModInfo.MODID + ":textures/entity/IonPlasma.png");
    }

    @Override
    public void doRender(Entity entity, double x, double y, double z, float f, float f1) {
        GL11.glPushMatrix();
        GL11.glTranslated(x, y, z);
        GL11.glDisable(GL11.GL_LIGHTING);
        GL11.glEnable(GL11.GL_BLEND);
        sphere.renderAll();
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 0.25F);
        GL11.glPopMatrix();
    }
}