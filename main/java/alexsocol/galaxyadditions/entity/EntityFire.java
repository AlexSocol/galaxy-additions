package alexsocol.galaxyadditions.entity;

import ibxm.Player;
import micdoodle8.mods.galacticraft.api.vector.Vector3;
import micdoodle8.mods.galacticraft.core.entities.EntityMeteorChunk;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityFireball;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.init.Blocks;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

public class EntityFire extends EntityThrowable {

	public EntityFire(World world, EntityLivingBase entity) {
		super(world, entity);
	}

	public EntityFire(World world) {
		super(world);
	}
	
	public EntityFire(World world, double x, double y, double z) {
		super(world, x, y, z);
    }
	
	@Override
	public void onImpact(MovingObjectPosition mop) {
		if(mop.typeOfHit.equals(MovingObjectPosition.MovingObjectType.ENTITY)){
			mop.entityHit.attackEntityFrom(DamageSource.causeMobDamage(getThrower()), 0.5F);
			mop.entityHit.setFire(5);
		}
		if(mop.typeOfHit.equals(MovingObjectPosition.MovingObjectType.BLOCK)){
			worldObj.setBlock(mop.blockX, mop.blockY + 1, mop.blockZ, Blocks.fire);
		}
	}
	@Override
	public void onUpdate(){
		super.onUpdate();
		
		this.motionY /= getGravityVelocity();

		if (this.isInWater() || this.isWet()) {
			this.setDead();
		}
	}
	
	@Override
	public float getGravityVelocity(){
        return 0.5F;
    }
	
	@Override
	public float func_70182_d() {
        return 3.5F;
    }

	@Override
    public float func_70183_g() {
        return 0.0F;
    }
}
