package alexsocol.galaxyadditions.entity;

import java.util.List;
import java.util.Random;

import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MathHelper;
import net.minecraft.world.EnumDifficulty;
import net.minecraft.world.World;

public class RedLightningEntity extends Entity {
	
    private int lightningState;
    public long boltVertex;
    private int boltLivingTime;

    private final Random random = new Random();
    
	private float damage;
    
	public RedLightningEntity(World world) {
        super(world);
    }
	
    public RedLightningEntity(World world, double X, double Y, double Z) {
        super(world);
        this.setSize(0.8F, 1.8F);
        this.setLocationAndAngles(X, Y, Z, 0.0F, 0.0F);
        this.lightningState = 2;
        this.boltVertex = this.rand.nextLong();
        this.boltLivingTime = this.rand.nextInt(3) + 2;

        if (!world.isRemote && world.getGameRules().getGameRuleBooleanValue("doFireTick") && (world.difficultySetting == EnumDifficulty.NORMAL || world.difficultySetting == EnumDifficulty.HARD) && world.doChunksNearChunkExist(MathHelper.floor_double(X), MathHelper.floor_double(Y), MathHelper.floor_double(Z), 10)) {
            int i = MathHelper.floor_double(X);
            int j = MathHelper.floor_double(Y);
            int k = MathHelper.floor_double(Z);

            if (world.getBlock(i, j, k).getMaterial() == Material.air && Blocks.fire.canPlaceBlockAt(world, i, j, k)) {
                world.setBlock(i, j, k, Blocks.fire);
            }

            for (i = 0; i < 4; ++i) {
                k = MathHelper.floor_double(X) + this.rand.nextInt(3) - 1;
                j = MathHelper.floor_double(Y) + this.rand.nextInt(3) - 1;
                int l = MathHelper.floor_double(Z) + this.rand.nextInt(3) - 1;

                if (world.getBlock(k, j, l).getMaterial() == Material.air && Blocks.fire.canPlaceBlockAt(world, k, j, l)) {
                    world.setBlock(k, j, l, Blocks.fire);
                }
            }
        }
    }


    public void onUpdate() {
        super.onUpdate();

        if (this.lightningState == 2) {
            this.worldObj.playSoundEffect(this.posX, this.posY, this.posZ, "ambient.weather.thunder", 10000.0F, 0.8F + this.rand.nextFloat() * 0.2F);
            this.worldObj.playSoundEffect(this.posX, this.posY, this.posZ, "random.explode", 2.0F, 0.5F + this.rand.nextFloat() * 0.2F);
        }

        --this.lightningState;

        if (this.lightningState < 0) {
            if (this.boltLivingTime == 0) {
                this.setDead();
            }
            else if (this.lightningState < -this.rand.nextInt(10)) {
                --this.boltLivingTime;
                this.lightningState = 1;
                this.boltVertex = this.rand.nextLong();

                if (!this.worldObj.isRemote && this.worldObj.getGameRules().getGameRuleBooleanValue("doFireTick") && this.worldObj.doChunksNearChunkExist(MathHelper.floor_double(this.posX), MathHelper.floor_double(this.posY), MathHelper.floor_double(this.posZ), 10)) {
                    int i = MathHelper.floor_double(this.posX);
                    int j = MathHelper.floor_double(this.posY);
                    int k = MathHelper.floor_double(this.posZ);

                    if (this.worldObj.getBlock(i, j, k).getMaterial() == Material.air && Blocks.fire.canPlaceBlockAt(this.worldObj, i, j, k)) {
                        this.worldObj.setBlock(i, j, k, Blocks.fire);
                    }
                }
            }
        }

        if (this.lightningState >= 0) {
        	double d0 = 3.0D;
            List list = this.worldObj.getEntitiesWithinAABBExcludingEntity(this, AxisAlignedBB.getBoundingBox(this.posX - d0, this.posY - d0, this.posZ - d0, this.posX + d0, this.posY + 6.0D + d0, this.posZ + d0));

            for (int l = 0; l < list.size(); ++l) {
                Entity entity = (Entity)list.get(l);
                    
                for (int i = 0; i < 6; i++) {
                	damage += ((float) (random.nextInt(7) + 1));
                }
                    
                if (entity instanceof EntityPlayer) {
                	EntityPlayer player = (EntityPlayer) entity;
					player.attackEntityFrom(DamageSource.onFire, damage);
				}
                    
				if (entity instanceof EntityLiving) {
					EntityLiving living = (EntityLiving) entity;
					living.attackEntityFrom(DamageSource.onFire, damage);
				}
				damage = 0.0F;
            }
        }
    }

    protected void entityInit() {}

    protected void readEntityFromNBT(NBTTagCompound nbt) {}

    protected void writeEntityToNBT(NBTTagCompound nbt) {}
}