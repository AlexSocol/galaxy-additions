package alexsocol.galaxyadditions.items;

import alexsocol.asjlib.ASJUtilities;
import alexsocol.galaxyadditions.ModInfo;
import alexsocol.galaxyadditions.entity.IonPlasmaBurstEntity;
import alexsocol.galaxyadditions.utils.RegistrationsList;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

public class IonPlasmaRifle extends WeaponBase {

	public IonPlasmaRifle() {
		super(8, 512, false, (AmmoBase) RegistrationsList.plasmaAmmo, "IonPlasmaRifle", 40, 80);
		this.setTextureName(ModInfo.MODID + ":" + "plasmaRifle");
	}

	@Override
	public void spawnProjectile(World world, EntityPlayer player) {
		MovingObjectPosition mop = ASJUtilities.getSelectedBlock(player, 1.0F, 256.0D, true);
		if (mop != null) {
			world.spawnEntityInWorld(new IonPlasmaBurstEntity(world, mop.blockX, mop.blockY, mop.blockZ, player));
		}
	}	
}
