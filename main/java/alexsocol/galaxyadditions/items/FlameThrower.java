package alexsocol.galaxyadditions.items;

import alexsocol.galaxyadditions.ModInfo;
import alexsocol.galaxyadditions.entity.EntityFire;
import alexsocol.galaxyadditions.utils.RegistrationsList;
import micdoodle8.mods.galacticraft.core.entities.EntityMeteor;
import net.minecraft.client.particle.EntityFlameFX;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.entity.projectile.EntityFireball;
import net.minecraft.entity.projectile.EntityLargeFireball;
import net.minecraft.item.Item;
import net.minecraft.world.World;

public class FlameThrower extends WeaponBase {

	public FlameThrower() {
		super(250, 16000, false, (AmmoBase) RegistrationsList.fuelTank, "FlameThrower", 60, 5);
		this.setTextureName(ModInfo.MODID + ":" + "flameThrower");
	}

	@Override
	public void spawnProjectile(World world, EntityPlayer player) {
		if(!world.isRemote){
		world.spawnEntityInWorld(new EntityFire(world, player));
		}
	}
}