package alexsocol.galaxyadditions.items;

import alexsocol.galaxyadditions.ModInfo;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import micdoodle8.mods.galacticraft.core.GalacticraftCore;
import micdoodle8.mods.galacticraft.core.energy.item.ItemElectricBase;
import micdoodle8.mods.galacticraft.core.proxy.ClientProxyCore;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemStack;

public class TitaniumBattery extends ItemElectricBase
{
    public TitaniumBattery(String assetName, CreativeTabs tab)
    {
        super();
        this.setUnlocalizedName(assetName);
        this.setCreativeTab(tab);
        this.setTextureName(ModInfo.MODID + ":" + assetName);
    }

    @Override
    public float getMaxElectricityStored(ItemStack itemStack)
    {
        return 80000;
    }
}
