package alexsocol.galaxyadditions.items;

import java.util.List;
import java.util.Random;

import alexsocol.galaxyadditions.GAMain;
import alexsocol.galaxyadditions.ModInfo;
import cpw.mods.fml.relauncher.*;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.util.StatCollector;

public class FloaterDrop extends Item {
	
	public static final String[] drops = new String[] { "FloaterGasBags", "FloaterHeart" };
	
	@SideOnly(Side.CLIENT)
	private IIcon[] texture;
	public static Random rand = new Random();
	
	public FloaterDrop() {
		this.setCreativeTab(GAMain.gaTab);
		this.setHasSubtypes(true);
		this.setMaxStackSize(1);
	}
	
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconRegister) {
		texture = new IIcon[drops.length];
		for (int i = 0; i < drops.length; i++){
			texture[i] = iconRegister.registerIcon(ModInfo.MODID + ":" + this.getUnlocalizedName().substring(5) + "." + drops[i]);
		}
	}

    @SideOnly(Side.CLIENT)
    public IIcon getIconFromDamage(int i) {
    	if (i < texture.length) {
        	return texture[i];
    	} else {
    		return texture[0];
    	}
    }

    @SideOnly(Side.CLIENT)
    public void getSubItems(Item item, CreativeTabs tab, List list) {
        for (int i = 0; i < drops.length; ++i) {
            list.add(new ItemStack(item, 1, i));
        }
    }

    public String getUnlocalizedName(ItemStack stack) {
    	if (stack.getItemDamage() < drops.length) {
        	return "item." + drops[stack.getItemDamage()];
    	} else {
    		return "item." + drops[0];
    	}
    }
}