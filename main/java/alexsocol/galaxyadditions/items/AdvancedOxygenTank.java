package alexsocol.galaxyadditions.items;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import micdoodle8.mods.galacticraft.core.GalacticraftCore;
import micdoodle8.mods.galacticraft.core.items.ItemOxygenTank;
import micdoodle8.mods.galacticraft.core.proxy.ClientProxyCore;
import micdoodle8.mods.galacticraft.core.util.GCCoreUtil;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.minecraftforge.common.util.EnumHelper;

import java.util.List;

import alexsocol.galaxyadditions.GAMain;
import alexsocol.galaxyadditions.ModInfo;

public class AdvancedOxygenTank extends ItemOxygenTank
	{
    public AdvancedOxygenTank(int tier, String assetName)
    {
        super(tier, assetName);
        this.setMaxStackSize(1);
        this.setMaxDamage(tier * 1080);
        this.setUnlocalizedName(assetName);
        this.setTextureName(ModInfo.MODID + ":" + assetName);
        this.setNoRepair();
        
    }
  
    @Override
    public CreativeTabs getCreativeTab()
    {
        return GAMain.gaTab;
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
    public void addInformation(ItemStack par1ItemStack, EntityPlayer player, List par2List, boolean b)
    {
        par2List.add(GCCoreUtil.translate("gui.tank.oxygenRemaining") + ": " + (par1ItemStack.getMaxDamage() - par1ItemStack.getItemDamage()));
    }
}