//- By Vamig Aliev.
//- https://vk.com/win_vista.

package vamig.worldengine;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import cpw.mods.fml.common.IWorldGenerator;
import net.minecraft.block.Block;
import net.minecraft.block.BlockFalling;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.ChunkProviderGenerate;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.terraingen.PopulateChunkEvent;

public class WE_ChunkProvider extends ChunkProviderGenerate {
	public World worldObj; // TODO try to make private
	protected Random rand;
	
	//////////////////
	//- Generators -//
	//////////////////
	protected List<IWorldGenerator		> createChunkGen_List       = new ArrayList();
	protected List<WE_CreateChunkGen_InXZ > createChunkGen_InXZ_List  = new ArrayList();
	//private List<WE_CreateChunkGen_InXYZ> createChunkGen_InXYZ_List = new ArrayList();
	protected List<IWorldGenerator        > decorateChunkGen_List     = new ArrayList();
	
	//////////////////////
	//- Biome Map Info -//
	//////////////////////
	protected List<WE_Biome> biomesList = new ArrayList();
	protected WE_Biome standardBiomeOnMap;
	//-//
	protected double biomemapPersistence = 1.0D, biomemapScaleX = 1.0D, biomemapScaleY = 1.0D;
	protected int biomemapNumberOfOctaves = 1;
	
	public WE_ChunkProvider(WE_WorldProvider wp) {
		super(wp.worldObj, wp.getSeed(), wp.worldObj.getWorldInfo().isMapFeaturesEnabled());
		worldObj = wp.worldObj;
		rand     = new Random(wp.getSeed());
		
		System.out.println("WorldEngine: -Applying your WorldEngine settings..."  );
		wp.genSettings(this);
		System.out.println("WorldEngine: -WorldEngine is configured successfully!");
	}
	
	/** Add generator (Caves, Ravines, etc) */
	public void addGenerator(IWorldGenerator gener) {
		createChunkGen_List.add(gener);
	}
	
	/** Add map decorator (Lakes, Ores, etc) */
	public void addDecorator(IWorldGenerator decor) {
		decorateChunkGen_List.add(decor);
	}
	
	/** Add biomes */
	public void addBiomes(WE_Biome biome) {
		biomesList.add(biome);
	}
	
	@Override
	public Chunk provideChunk(int chunkX, int chunkZ) {
		long chunk_X = (long)chunkX * 16L, chunk_Z = (long)chunkZ * 16L;
		Block[] chunkBlocks     = new Block[65536];
		byte [] chunkBlocksMeta = new byte [65536];
		rand.setSeed(worldObj.getSeed() * chunkX ^ 3 + chunkZ ^ 2 * 9874L + 7684053L);
		//-//
		WE_Biome[][] chunkBiomes = new WE_Biome[16][16];
		for(int x = 0; x < 16; x++)
			for(int z = 0; z < 16; z++)
				chunkBiomes[x][z] = getBiomeAt(this, chunk_X + (long)x, chunk_Z + (long)z);
		
		/////
		//=//
		/////
		
		for(int i = 0; i < createChunkGen_List.size(); i++)
			((WE_CreateChunkGen) createChunkGen_List.get(i)).gen(new WE_GeneratorData(this, chunkBlocks, chunkBlocksMeta, chunk_X, chunk_Z, chunkBiomes, 0, 0, 0));
		//-//
		for(int x = 0; x < 16; x++)
			for(int z = 0; z < 16; z++) {
				for(int i = 0; i < createChunkGen_InXZ_List                  .size(); i++)
					createChunkGen_InXZ_List                  .get(i).gen(new WE_GeneratorData(this, chunkBlocks, chunkBlocksMeta, chunk_X, chunk_Z, chunkBiomes, x, 0, z));
				for(int i = 0; i < chunkBiomes[x][z].createChunkGen_InXZ_List.size(); i++)
					chunkBiomes[x][z].createChunkGen_InXZ_List.get(i).gen(new WE_GeneratorData(this, chunkBlocks, chunkBlocksMeta, chunk_X, chunk_Z, chunkBiomes, x, 0, z));
				//-//
				/*for(int y = 255; y >= 0; y--) {
					for(int i = 0; i < createChunkGen_InXYZ_List.size(); i++)
						createChunkGen_InXYZ_List                  .get(i).gen(new WE_GeneratorData(this, chunkBlocks, chunkBlocksMeta, chunk_X, chunk_Z, chunkBiomes, x, y, z));
					for(int i = 0; i < chunkBiomes[x][z].createChunkGen_InXYZ_List.size(); i++)
						chunkBiomes[x][z].createChunkGen_InXYZ_List.get(i).gen(new WE_GeneratorData(this, chunkBlocks, chunkBlocksMeta, chunk_X, chunk_Z, chunkBiomes, x, y, z));
				}*/
			}
		
		/////
		//=//
		/////
		
		WE_ChunkSmartLight chunk = new WE_ChunkSmartLight(worldObj, chunkBlocks, chunkBlocksMeta, chunkX, chunkZ);
		chunk.generateSkylightMap();
        return chunk;
	}
	
	@Override
	public void populate(IChunkProvider chunkProvider, int chunkX, int chunkZ) {
		BlockFalling.fallInstantly =  true;
		//-//
		boolean flag = false;
		rand.setSeed(worldObj.getSeed() * chunkX + chunkZ ^ 2 * 107L + 2394720L);
		MinecraftForge.EVENT_BUS.post(new PopulateChunkEvent.Pre (chunkProvider, worldObj, rand, chunkX, chunkZ, flag));
		
		/////
		//=//
		/////
		
		for(int i = 0; i <   decorateChunkGen_List.size(); i++)
			decorateChunkGen_List  .get(i).generate(rand, chunkX, chunkZ, worldObj, this, this);
		//-//
		WE_Biome b = getBiomeAt(this, (long)chunkX * 16L + (long)rand.nextInt(16), (long)chunkZ * 16L + (long)rand.nextInt(16));
		for(int i = 0; i < b.decorateChunkGen_List.size(); i++)
			b.decorateChunkGen_List.get(i).generate(rand, chunkX, chunkZ, worldObj, this, this);
		
		/////
		//=//
		/////
		
		MinecraftForge.EVENT_BUS.post(new PopulateChunkEvent.Post(chunkProvider, worldObj, rand, chunkX, chunkZ, flag));
		//-//
		BlockFalling.fallInstantly = false;
	}
	
	public void genSetBlock(Block[] chunkBlocks, byte[] chunkBlocksMeta, int x, int y, int z, Block block, byte meta) {
		if(x >= 0 && x <= 15 && y >= 0 && y <= 255 && z >= 0 && z <= 15) {
			chunkBlocks    [(x * 16 + z) * 256 + y] = block;
			chunkBlocksMeta[(x * 16 + z) * 256 + y] =  meta;
		}
	}
	
	public Block genReturnBlock    (Block[] chunkBlocks    , int x, int y, int z) {
		if(x >= 0 && x <= 15 && y >= 0 && y <= 255 && z >= 0 && z <= 15)
			return chunkBlocks    [(x * 16 + z) * 256 + y];
		else
			return null;
	}
	
	public byte  genReturnBlockMeta(byte [] chunkBlocksMeta, int x, int y, int z) {
		if(x >= 0 && x <= 15 && y >= 0 && y <= 255 && z >= 0 && z <= 15)
			return chunkBlocksMeta[(x * 16 + z) * 256 + y];
		else
			return    0;
	}
	
	public static void setBiomeMap(WE_ChunkProvider cp, double persistence, int numOctaves, double sx, double sy) {
		cp.biomemapPersistence     = persistence;
		cp.biomemapNumberOfOctaves =  numOctaves;
		cp.biomemapScaleX          =          sx;
		cp.biomemapScaleY          =          sy;
	}
	
	public static int addBiomeToGeneration(WE_ChunkProvider cp, WE_Biome biome) {
		if(biome.biomeMaxValueOnMap < biome.biomeMinValueOnMap) {
			double                 t = biome.biomeMaxValueOnMap;
			biome.biomeMaxValueOnMap = biome.biomeMinValueOnMap;
			biome.biomeMinValueOnMap =                        t;
		}
		//-//
		cp.biomesList.add(biome);
		biome.id = cp.biomesList.size();
		//-//
		if(cp.standardBiomeOnMap == null)
			cp.standardBiomeOnMap = biome;
		//-//
		return biome.id;
	}
	
	public static WE_Biome getBiomeAt(WE_ChunkProvider cp, long x, long z) {
		double biomeMapData = WE_PerlinNoise.PerlinNoise2D((cp.worldObj.getSeed() * 11) ^ 6,
			x / cp.biomemapScaleX, z / cp.biomemapScaleX,
			cp.biomemapPersistence, cp.biomemapNumberOfOctaves)
			* cp.biomemapScaleY;
		//-//
		WE_Biome r = null;
		for(int i = 0; i < cp.biomesList.size(); i++)
			if(biomeMapData >= cp.biomesList.get(i).biomeMinValueOnMap && biomeMapData <= cp.biomesList.get(i).biomeMaxValueOnMap)
				if(r != null) {
					if(cp.biomesList.get(i).biomeMaxValueOnMap - cp.biomesList.get(i).biomeMinValueOnMap < r.biomeMaxValueOnMap - r.biomeMinValueOnMap)
						r = cp.biomesList.get(i);
				}else
					r = cp.biomesList.get(i);
		//-//
		if(r == null)
			r = cp.standardBiomeOnMap;
		return r;
	}
	
	public static int getBiggestInterpolationQuality(WE_ChunkProvider cp) {
		int r = 0;
		for(int i = 0; i < cp.biomesList.size(); i++)
			if(cp.biomesList.get(i).biomeInterpolateQuality > r)
				r = cp.biomesList.get(i).biomeInterpolateQuality;
		return r;
	}
}