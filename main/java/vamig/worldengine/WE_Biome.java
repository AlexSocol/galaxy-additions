//- By Vamig Aliev.
//- https://vk.com/win_vista.

package vamig.worldengine;

import java.util.ArrayList;
import java.util.List;

import cpw.mods.fml.common.IWorldGenerator;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.biome.BiomeGenBase;

public class WE_Biome extends BiomeGenBase {
	public int id;
	//////////////////
	//- Biome Info -//
	//////////////////
	protected double
		biomeMinValueOnMap = -1.0D,
		biomeMaxValueOnMap =  1.0D,
		biomePersistence = 1.0D,
		biomeScaleX = 1.0D, biomeScaleY = 1.0D;
	protected int biomeNumberOfOctaves = 1,
		biomeSurfaceHeight = 63,
		biomeInterpolateQuality = 16;

	//////////////////
	//- Generators -//
	//////////////////
	protected List<WE_CreateChunkGen_InXZ > createChunkGen_InXZ_List  = new ArrayList();
	protected List<IWorldGenerator        > decorateChunkGen_List     = new ArrayList();

	/** The main constructor */
	public WE_Biome(int ID_FOR_ALL_WE_BIOMES, boolean r) {
		super(ID_FOR_ALL_WE_BIOMES, r);
		this.setBiomeName("WorldEngine");
		this.spawnableCaveCreatureList .clear();
		this.spawnableCreatureList     .clear();
		this.spawnableMonsterList      .clear();
		this.spawnableWaterCreatureList.clear();
			
		this.biomeMinValueOnMap = -1.0D;
		this.biomeMaxValueOnMap = 1.0D;
		this.biomePersistence = 1.0D;
		this.biomeScaleX = 1.0D;
		this.biomeScaleY = 1.0D;
		this.biomeNumberOfOctaves = 1;
		this.biomeSurfaceHeight = 63;
		this.biomeInterpolateQuality = 16;
	}
	
	public WE_Biome(int ID_FOR_ALL_WE_BIOMES) {
		this(ID_FOR_ALL_WE_BIOMES, false);
	}
	
	public WE_Biome(int ID_FOR_ALL_WE_BIOMES, double minMapValue, double maxMapValue, double persistence, int numOctaves, double sx, double sy, int height, int interpolateQuality, WE_BiomeLayer standardBiomeLayers) {
		this(ID_FOR_ALL_WE_BIOMES);
		this.biomeMinValueOnMap = minMapValue;
		this.biomeMaxValueOnMap = maxMapValue;
		this.biomePersistence        =        persistence;
		this.biomeNumberOfOctaves    =         numOctaves;
		this.biomeScaleX             =                 sx;
		this.biomeScaleY             =                 sy;
		this.biomeSurfaceHeight      =             height;
		this.biomeInterpolateQuality = interpolateQuality;
	}

	public void addLayerGenerator(WE_CreateChunkGen_InXZ layer) {
		createChunkGen_InXZ_List.add(layer);
	}
	
	public void addDecorations(IWorldGenerator decor) {
		decorateChunkGen_List.add(decor);
	}
}