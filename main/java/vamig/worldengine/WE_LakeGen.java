//- By Vamig Aliev.
//- https://vk.com/win_vista.

package vamig.worldengine;

import java.util.Random;

import cpw.mods.fml.common.IWorldGenerator;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;

public class WE_LakeGen implements IWorldGenerator {
	/** Main block of this lake */
	private Block lakeBlock;
	/** Top cover block of this lake */
	private Block topBlock;
	/** Main block metadata */
	private byte  lakeBlockMeta;
	/** Top cover block metadata */
	private byte  topBlockMeta;
	/** How rare this lake is <br> Ex: 12 means one lake per 12 chunks */
	private int chance;
	/** Min Y value to generate lake on */
	private int minY;
	/** Max Y value to generate lake on */
	private int maxY;
	/** Starting Y value to generate top blocks */
	private int fY;
	/** Possible offset for top block */
	private int random_fY;
	/** enerate top block */
	private boolean fGen;
	
	public WE_LakeGen(Block main, byte mainMeta, Block top, byte topMeta, int chanceToGen, int minY, int maxY, int topY, int topOffset, boolean genTop) {
		this.lakeBlock = main;
		this.lakeBlockMeta = mainMeta;
		this.topBlock = top;
		this.topBlockMeta = topMeta;
		this.chance = chanceToGen;
		this.minY = minY;
		this.maxY = maxY;
		this.fY = topY;
		this.random_fY = topOffset;
		this.fGen = genTop;
	}
	
	@Override
	public void generate(Random random, int chunkX, int chunkZ, World world, IChunkProvider chunkGenerator, IChunkProvider chunkProvider) {
		if(random.nextInt(chance) == 0) {
			int x = chunkX * 16 + random.nextInt(16),
				z = chunkZ * 16 + random.nextInt(16),
				y = minY + random.nextInt(maxY - minY + 1);
			//-//
			lake(world, random, x, y, z);
		}
	}
	
	public void lake(World world, Random random, int x, int y, int z) {
		x -= 8;
		z -= 8;
		while(y > 5 && world.isAirBlock(x, y, z))
			--y;
		
		if(y <= 4)
			return;
		else {
			y -= 4;
			
			boolean[] aboolean = new boolean[2048];
			for(int w = 0; w < random.nextInt(4) + 4; ++w) {
				double
					d0 = random.nextDouble() * 6 + 3,
					d1 = random.nextDouble() * 4 + 2,
					d2 = random.nextDouble() * 6 + 3,
					d3 = random.nextDouble() * (14 - d0) + 1 + d0 / 2,
					d4 = random.nextDouble() * ( 4 - d1) + 2 + d1 / 2,
					d5 = random.nextDouble() * (14 - d2) + 1 + d2 / 2;
				//-//
				for(int bx = 1; bx < 15; ++bx)
					for(int bz = 1; bz < 15; ++bz)
						for(int by = 1; by < 7; ++by) {
							double
								d6 = ((double)bx - d3) * 2 / d0,
								d7 = ((double)by - d4) * 2 / d1,
								d8 = ((double)bz - d5) * 2 / d2,
								d9 = d6 * d6 + d7 * d7 + d8 * d8;
							//-//
							if(d9 < 1)
								aboolean[(bx * 16 + bz) * 8 + by] = true;
						}
			}
			
			for(int bx = 0; bx < 16; ++bx)
				for(int bz = 0; bz < 16; ++bz) {
					for(int by = 0; by < 8; ++by) {
						if(!aboolean[(bx * 16 + bz) * 8 + by] && (
							bx < 15 && aboolean[((bx + 1) * 16 + bz    ) * 8 + by    ] || bx > 0 && aboolean[((bx - 1) * 16 + bz    ) * 8 + by    ] ||
							bz < 15 && aboolean[( bx      * 16 + bz + 1) * 8 + by    ] || bz > 0 && aboolean[( bx      * 16 + bz - 1) * 8 + by    ] ||
							by <  7 && aboolean[( bx      * 16 + bz    ) * 8 + by + 1] || by > 0 && aboolean[( bx      * 16 + bz    ) * 8 + by - 1])) {
							Material material = world.getBlock(x + bx, y + by, z + bz).getMaterial();
							if(by >= 4 &&  material.isLiquid()                                                                                           )
								return;
							if(by <  4 && !material.isSolid () &&
								!(world.getBlock(x + bx, y + by, z + bz) == lakeBlock && world.getBlockMetadata(x + bx, y + by, z + bz) == lakeBlockMeta))
								return;
						}
						//-//
						if( aboolean[(bx * 16 + bz) * 8 + by]                                                                                       )
							world.setBlock(x + bx, y + by, z + bz, by >= 4 ? Blocks.air : lakeBlock,   lakeBlockMeta, 3);
					}
					if(y + 4 >= fY + random.nextInt(random_fY + 1))
						world    .setBlock(x + bx, y +  4, z + bz,                      topBlock, topBlockMeta, 3);
				}
		}
	}
}